# Setting up
------------

1. Start up Vivado and load in the project in FPGA/projects
2. Start Xilinx SDK from this project
3. Flash the Zybo with the bitstream already provided by Vivado to the SDK
4. Run app_1 in debug mode on the zybo
5. Make sure you are on the same subnet as the zybo (192.168.1.10 255:255:255:0)
6. Run the application and start up both a telnet session with the zybo and a
   serial session
7. Once connected to the Zybo it is possible to send it commands in this format
   :
   ```
   x y val
   ```
   Where x is the row you want to access
         y the column
         and val the value you want to put on those coordinates.
8. When and invalid input is given the server will respond with an error 

## Example input
```
$ telnet 192.168.1.10 7

1 5 BLUE    # Will set row 1 column 5 on a BLUE color
50 1 YELLOW # Will return a warning since 50 > 7 which is the limit of our
            # matrix.
[ERROR]
```

## Color list

* RED
* BLUE
* GREEN
* BLACK
* WHITE
* PURPLE
* YELLOW
