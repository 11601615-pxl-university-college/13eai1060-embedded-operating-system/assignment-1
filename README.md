# Embedded Operating Systems Assignment 1 

Assignment 1 of the Embedded Operating Systems course lectured by [Vincent Claes](https://www.linkedin.com/in/vincentclaes/) @ [PXL University College](https://www.pxl.be).

## Source Road map

```
FPGA             The Vivado 2018.2 Project
other            Directory containing Other software needed for running the application
documentation    Directory containing very short tutorial on how to start the application
misc             Miscellaneous
```

## System Overview

<p align="center"><img src="./misc/systemoverview.png"></p>

## Project Goals

* For this project you have to use your Xilinx Zybo board where you program 2 AXI IP Block for
  controlling a 8*8 Neopixel matrix (WS2812) and a VGA screen.
* The output on tgge Screen and Neopixel has to be the same, so you have to divide your VGA
  screen in 8*8 blocks.
* For controlling your application you have to use Network communication
* You can develop your own application but you have to use FreeRTOS with Threads, Queues, …
    - For instance: pong with udp control, Scrolling text display,…

## Built With

* [Vivado](https://www.xilinx.com/products/design-tools/vivado.html) - The IDE used
* [Zybo](https://store.digilentinc.com/zybo-zynq-7000-arm-fpga-soc-trainer-board/) - The development board used 
* [FreeRTOS](https://www.freertos.org/) - The RTOS used
* [NeoMatrix 8x8](https://www.adafruit.com/product/1487) - The 8x8 NeoMatrix used

## Authors

* **Vincent Claes**    - *Lecturer* - [LinkedIn](https://www.linkedin.com/in/vincentclaes/)
* **Jeffrey Gorissen** - *Student*  - [LinkedIn](https://www.linkedin.com/in/jeffrey-gorissen-6120a2142/)
* **Bryan Honof**      - *Student*  - [LinkedIn](https://www.linkedin.com/in/bryan-honof/)
* **Joeri Rethy**      - *Student*  - [LinkedIn]()

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration


