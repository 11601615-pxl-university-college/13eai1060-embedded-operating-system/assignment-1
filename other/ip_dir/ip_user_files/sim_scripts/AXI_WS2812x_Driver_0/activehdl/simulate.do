onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+AXI_WS2812x_Driver_0 -L xil_defaultlib -L secureip -O5 xil_defaultlib.AXI_WS2812x_Driver_0

do {wave.do}

view wave
view structure

do {AXI_WS2812x_Driver_0.udo}

run -all

endsim

quit -force
