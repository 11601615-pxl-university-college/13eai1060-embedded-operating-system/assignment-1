library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity vga_grid is
	port 
	( 
		vidon : in std_logic;
		hc    : in std_logic_vector(9 downto 0);
		vc    : in std_logic_vector(9 downto 0);
		
		red   : out std_logic_vector(4 downto 0);
		green : out std_logic_vector(5 downto 0);
		blue  : out std_logic_vector(4 downto 0);
		
		-- AXI registers
		-- Row 0
		PIX_0_0 : in std_logic_vector(11 downto 0);
		PIX_0_1 : in std_logic_vector(11 downto 0);
		PIX_0_2 : in std_logic_vector(11 downto 0);
		PIX_0_3 : in std_logic_vector(11 downto 0);
		PIX_0_4 : in std_logic_vector(11 downto 0);
		PIX_0_5 : in std_logic_vector(11 downto 0);
		PIX_0_6 : in std_logic_vector(11 downto 0);
		PIX_0_7 : in std_logic_vector(11 downto 0);
		
		-- Row 1
		PIX_1_0 : in std_logic_vector(11 downto 0);
		PIX_1_1 : in std_logic_vector(11 downto 0);
		PIX_1_2 : in std_logic_vector(11 downto 0);
		PIX_1_3 : in std_logic_vector(11 downto 0);
		PIX_1_4 : in std_logic_vector(11 downto 0);
		PIX_1_5 : in std_logic_vector(11 downto 0);
		PIX_1_6 : in std_logic_vector(11 downto 0);
		PIX_1_7 : in std_logic_vector(11 downto 0);
		
		-- Row 2
		PIX_2_0 : in std_logic_vector(11 downto 0);
		PIX_2_1 : in std_logic_vector(11 downto 0);
		PIX_2_2 : in std_logic_vector(11 downto 0);
		PIX_2_3 : in std_logic_vector(11 downto 0);
		PIX_2_4 : in std_logic_vector(11 downto 0);
		PIX_2_5 : in std_logic_vector(11 downto 0);
		PIX_2_6 : in std_logic_vector(11 downto 0);
		PIX_2_7 : in std_logic_vector(11 downto 0);
		
		-- Row 3
		PIX_3_0 : in std_logic_vector(11 downto 0);
		PIX_3_1 : in std_logic_vector(11 downto 0);
		PIX_3_2 : in std_logic_vector(11 downto 0);
		PIX_3_3 : in std_logic_vector(11 downto 0);
		PIX_3_4 : in std_logic_vector(11 downto 0);
		PIX_3_5 : in std_logic_vector(11 downto 0);
		PIX_3_6 : in std_logic_vector(11 downto 0);
		PIX_3_7 : in std_logic_vector(11 downto 0);
		
		-- Row 4
		PIX_4_0 : in std_logic_vector(11 downto 0);
		PIX_4_1 : in std_logic_vector(11 downto 0);
		PIX_4_2 : in std_logic_vector(11 downto 0);
		PIX_4_3 : in std_logic_vector(11 downto 0);
		PIX_4_4 : in std_logic_vector(11 downto 0);
		PIX_4_5 : in std_logic_vector(11 downto 0);
		PIX_4_6 : in std_logic_vector(11 downto 0);
		PIX_4_7 : in std_logic_vector(11 downto 0);
		
		-- Row 5
		PIX_5_0 : in std_logic_vector(11 downto 0);
		PIX_5_1 : in std_logic_vector(11 downto 0);
		PIX_5_2 : in std_logic_vector(11 downto 0);
		PIX_5_3 : in std_logic_vector(11 downto 0);
		PIX_5_4 : in std_logic_vector(11 downto 0);
		PIX_5_5 : in std_logic_vector(11 downto 0);
		PIX_5_6 : in std_logic_vector(11 downto 0);
		PIX_5_7 : in std_logic_vector(11 downto 0);
		
		-- Row 6
		PIX_6_0 : in std_logic_vector(11 downto 0);
		PIX_6_1 : in std_logic_vector(11 downto 0);
		PIX_6_2 : in std_logic_vector(11 downto 0);
		PIX_6_3 : in std_logic_vector(11 downto 0);
		PIX_6_4 : in std_logic_vector(11 downto 0);
		PIX_6_5 : in std_logic_vector(11 downto 0);
		PIX_6_6 : in std_logic_vector(11 downto 0);
		PIX_6_7 : in std_logic_vector(11 downto 0);
		
		-- Row 7
		PIX_7_0 : in std_logic_vector(11 downto 0);
		PIX_7_1 : in std_logic_vector(11 downto 0);
		PIX_7_2 : in std_logic_vector(11 downto 0);
		PIX_7_3 : in std_logic_vector(11 downto 0);
		PIX_7_4 : in std_logic_vector(11 downto 0);
		PIX_7_5 : in std_logic_vector(11 downto 0);
		PIX_7_6 : in std_logic_vector(11 downto 0);
		PIX_7_7 : in std_logic_vector(11 downto 0)
	);
end vga_grid;

architecture Behavioral of vga_grid is

begin

	red <= (others => '1');
	green <= (others => '1');
	blue <= (others => '1');

end Behavioral;
